https://github.com/mgramin/awesome-db-tools

# Vrais Concurrent
- datagrip
- dbForge
- dbeaver / CloudBeaver
- navicat
- sqldbm
- ERBuilder Data Modeler
- beekeeper
- tableplus
- SQLGate
- dbvis
- Aqua Data Studio


# Faux Concurrent
- pgmodeler
- studio3t
- pgadmin
- phpmyadmin
- mysqlworkbench
- mongodb compass
- Text2SQL.AI
- AI2sql
- AIHelperBot
- Blaze SQL
